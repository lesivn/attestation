<?php

namespace App\Http\Filters;

use EloquentFilter\ModelFilter;
use Illuminate\Support\Str;

class CarFilter extends ModelFilter
{

    public function query(string $query)
    {
        $query = Str::lower($query);
        return $this->whereRaw('LOWER(vin) LIKE ?', ["%$query%"])
            ->orWhereRaw('LOWER(number) LIKE ?', ["%$query%"])
            ->orWhereRaw('LOWER(name) LIKE ?', ["%$query%"]);
    }

    public function vendor(string $vendor)
    {
        $vendor = Str::lower($vendor);
        return $this->whereRaw('LOWER(vendor) LIKE ?', ["%$vendor%"]);
    }

    public function model(string $model)
    {
        $model = Str::lower($model);
        return $this->whereRaw('LOWER(model) LIKE ?', ["%$model%"]);
    }

    public function year(string $year)
    {
        return $this->where('year', $year);
    }

    public function sort(string $sort)
    {
        if (!strripos($sort, '-')){
            return $this;
        }

        list($column, $direction) = explode('-', $sort);

        return $this->orderBy($column, $direction);
    }
}
