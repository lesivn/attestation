<?php

namespace App\Http\Filters;

use EloquentFilter\ModelFilter;
use Illuminate\Support\Str;

class MakeFilter extends ModelFilter
{

    public function query(string $query)
    {
        $query = Str::lower($query);
        return $this->whereRaw('LOWER(make_name) LIKE ?', ["%$query%"]);
    }
}
