<?php

namespace App\Http\Controllers;

use App\CarModel;
use App\Http\Filters\MakeFilter;
use App\Make;
use Illuminate\Http\Request;

class MakeController extends Controller
{
    public function make()
    {
        $makes = Make::filter(request()->only(['query']), MakeFilter::class)->limit(10)->get();
        return $makes;
    }
}
