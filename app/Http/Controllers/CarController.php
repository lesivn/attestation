<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Requests\CarRequest;
use App\Http\Resources\CarCollection;
use App\Http\Resources\CarResource;
use Rap2hpoutre\FastExcel\FastExcel;
use GuzzleHttp\Client;

class CarController extends Controller
{
    public function index()
    {
        return new CarCollection(Car::getList());
    }

    public function store(CarRequest $request)
    {
        $car = Car::query()->create($request->validated());

        $client = new Client();
        $responseForVin = $client->get('https://vpic.nhtsa.dot.gov/api/vehicles/decodevinvalues/' . $car->vin . '?format=json');

        if ($responseForVin->getStatusCode() == 200) {
            $resultArray = json_decode($responseForVin->getBody());
            if (count($resultArray->Results)) {
                $resutlt = $resultArray->Results[0];
                $car->vendor = $resutlt->Make;
                $car->model = $resutlt->Model;
                $car->year = $resutlt->ModelYear;

                $car->save();
            }
        }

        return new CarResource($car);
    }

    public function show(Car $car)
    {
        return $car = Car::findOrFail($car);
    }

    public function update(CarRequest $request, $id)
    {
        $car = Car::findOrFail($id);
        $car->fill($request->all());
        $car->save();
        $client = new Client();
        $responseForVin = $client->get('https://vpic.nhtsa.dot.gov/api/vehicles/decodevinvalues/' . $car->vin . '?format=json');

        if ($responseForVin->getStatusCode() == 200) {
            $resultArray = json_decode($responseForVin->getBody());
            if (count($resultArray->Results)) {
                $resutlt = $resultArray->Results[0];
                $car->vendor = $resutlt->Make;
                $car->model = $resutlt->Model;
                $car->year = $resutlt->ModelYear;
                $car->save();
            }
        }

        return new CarResource($car);
    }

    public function destroy($id)
    {
        Car::query()->where('id', $id)->delete();
        return response(null, 204);
    }

    public function export(){
        $list = Car::getListForExport();
        return (new FastExcel($list))->download('file.xlsx');
    }
}
