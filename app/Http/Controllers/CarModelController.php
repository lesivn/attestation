<?php

namespace App\Http\Controllers;

use App\CarModel;
use Illuminate\Http\Request;

class CarModelController extends Controller
{

    public function index($make_id)
    {
        return CarModel::query()->whereMakeId($make_id)->get();
    }
}
