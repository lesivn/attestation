<?php

namespace App\Http\Requests;

use App\Car;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $car = $this->route('car');

        $rules = [
            'name' => 'required|string',
            'number' => 'required|string|max:10',
            'color' => 'required|string|max:255',
            'vin' => ['required', 'string', 'size:17', Rule::unique('cars')->ignore($car ?: null)]
        ];

        return $rules;

    }
}
