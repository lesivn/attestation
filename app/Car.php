<?php

namespace App;

use App\Http\Filters\CarFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use Filterable;

    protected $fillable = ['name', 'number', 'color', 'vin', 'vendor', 'model', 'year'];

    protected $hidden = ['created_at', 'updated_at'];

    public static function getList()
    {
        return self::filter(request()->only('year', 'model', 'vendor', 'sort'), CarFilter::class)
            ->paginate(10);
    }

    public static function getListForExport()
    {
        return self::filter(request()->only('year', 'model', 'vendor', 'sort'), CarFilter::class)
            ->get();
    }
}
