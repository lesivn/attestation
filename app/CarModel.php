<?php

namespace App;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Yadakhov\InsertOnDuplicateKey;

class CarModel extends Model
{

    use Filterable, InsertOnDuplicateKey;

    protected $fillable = ['make_id', 'model_name', 'model_id'];

    protected $hidden = ['created_at', 'updated_at'];
}
