<?php

namespace App;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Yadakhov\InsertOnDuplicateKey;

class Make extends Model
{

    use Filterable, InsertOnDuplicateKey;

    protected $fillable = ['make_id', 'make_name'];

    protected $hidden = ['created_at', 'updated_at'];
}
