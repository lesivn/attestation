<?php

namespace App\Console\Commands;

use App\CarModel;
use App\Make;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class UpdateModels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-car-models';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update models';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client();
        $responseMakes = $client->get('https://vpic.nhtsa.dot.gov/api/vehicles/getallmakes?format=json');

        if ($responseMakes->getStatusCode() == 200){
            $resultArray = json_decode($responseMakes->getBody());
            if(count($resultArray->Results)){
                foreach ($resultArray->Results as $make){
                    $item['make_id'] = $make->Make_ID;
                    $item['make_name'] = $make->Make_Name;
                    $items[] = $item;
                }
            }
        }

        Make::insertOnDuplicateKey(array_values($items), ['make_id', 'make_name']);

        foreach ($items as $make){
            $itemsModel = [];
            $responseModels = $client->get('https://vpic.nhtsa.dot.gov/api/vehicles/getmodelsformakeid/'.$make['make_id'].'?format=json');

            if ($responseModels->getStatusCode() == 200){
                $resultArray = json_decode($responseModels->getBody());
                if(count($resultArray->Results)){
                    foreach ($resultArray->Results as $make){
                        $itemModel['make_id'] = $make->Make_ID;
                        $itemModel['model_id'] = $make->Model_ID;
                        $itemModel['model_name'] = $make->Model_Name;
                        $itemsModel[] = $itemModel;
                    }
                    CarModel::insertOnDuplicateKey(array_values($itemsModel), ['make_id', 'model_id', 'model_name']);
                }
            }
        }

    }
}
