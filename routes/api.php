<?php

use Illuminate\Http\Request;

Route::apiResource('/car', 'CarController');
Route::get('export', 'CarController@export');

Route::get('get-make', 'MakeController@make');
Route::get('get-models/{make_id}', 'CarModelController@index');
